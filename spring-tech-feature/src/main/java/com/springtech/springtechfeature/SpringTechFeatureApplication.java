package com.springtech.springtechfeature;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringTechFeatureApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringTechFeatureApplication.class, args);
	}

}
